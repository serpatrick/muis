class Button {
  isDown: boolean;
  isPressed: boolean;

  constructor() {
    this.isDown = false;
    this.isPressed = false;
  }
}

const button = new Button();

function update() {
  button.isPressed = false;
}

function isDown() {
  return button.isDown;
}

function isPressed() {
  return button.isPressed;
}

function onMouseDown() {
  if (!button || button.isDown) {
    return;
  }

  button.isDown = true;
  button.isPressed = true;
}

function onMouseUp() {
  if (!button) {
    return;
  }

  button.isDown = false;
  button.isPressed = false;
}

window.addEventListener('mousedown', onMouseDown);
window.addEventListener('mouseup', onMouseUp);

export default {
  update,
  isDown,
  isPressed,
};
